import os
from typing import List

import pyshacl
import toml

SETTINGS = toml.load('acquisition_settings.toml')

SHAPE_FILES = SETTINGS['shape_files']
TERMS_FILES = SETTINGS['terms_files']
DATA_FILE = SETTINGS['data_file']

prefixes = []
shapes = []
terms = []
data = []


def parse_file(filename: str, prefixes: List[str], rest: List[str]):
    with open(filename, 'r') as infile:
        for line in infile:
            if '@prefix' in line:
                if line not in prefixes:
                    prefixes += [line]
            else:
                rest += [line]
    return prefixes, rest


def load_shapes(filepaths: List[List[str]]):
    global prefixes, shapes

    for filepath in filepaths:
        prefixes, shapes = parse_file(
            os.path.join('..', 'shapes', *filepath), prefixes, shapes)


def load_terms(filepaths: List[str]):
    global prefixes, terms

    for filepath in filepaths:
        prefixes, terms = parse_file(os.path.join( '..', 'terms', filepath), prefixes, terms)


if __name__ == '__main__':
    load_shapes(SHAPE_FILES)

    with open('shapes.ttl', 'w') as outfile:
        outfile.write(''.join(prefixes))
        outfile.write(''.join(shapes))

    load_terms(TERMS_FILES)

    with open('terms.ttl', 'w') as outfile:
        outfile.write(''.join(prefixes))
        outfile.write(''.join(terms))

    prefixes, data = parse_file(os.path.join(
        '..', 'examples', DATA_FILE), prefixes, data)
    with open('data.ttl', 'w') as outfile:
        outfile.write(''.join(prefixes))
        outfile.write(''.join(data))

    _, _, report = pyshacl.validate('data.ttl',
                                    shacl_graph='shapes.ttl',
                                    ont_graph='terms.ttl',
                                    inference='rdfs',
                                    do_owl_imports=True
                                    )
    print(report)
