# Flagship Use-Case *Virtual Climatization*

The files of the flagship use case can be found in the *shapes* directory.
The main "entry-point"-shape can be found [here](../shapes/virtual-climatization/use-case.ttl)

The superordinate goal of the WZL's research on *Virtual Climatization* is the data-driven online compensation of the volumetric error caused by thermal effects of a machine tool. The compensation is based on the measured temperature of the individiual axes and parts of the machine tool from which the expansion or contraction of such axes and parts is computed. For that, we use a mixed approach of black- and white-box machine learning models.

The overall data "pipeline" of the use-case *Virtual Climatization* can be seen in the figure below. Based on extensive data acquisition in long term *experiments*, the *calibration* of the machine tool results in a parametrized machine model, which allows for compensating the volumetric error caused by thermal effects.

![Virtual Climatization Pipeline](../assets/vc_experiment.png)

The *flagship application profile (FAP)* covers the *data acquisition*, i.e. the step *experiment* of the pipeline. The physical setup of this experiment is depicted below. The *set points* of the machine tool, i.e. its *tool center poin (TCP)* are predefined, whereas the real position of the TCP is measured with an API Radian laser tracker, used for validation and training the machine learning models. The temperature data is acquired by multiple self developed temperature sensors, which are distributed all over the machine and measure the temperature of individual parts of the machine tool. To simulate external heating sources, the portal is equipped with several heatpads, which cause a expansion of the machines X-axis (and also influencing the Y- and Z-Axis of the machine).

![Virtual Climatization Experiment](../assets/vc_setup.png)

A simple visualition of the resulting AP is shown below, the source files can be viewed in the accompanying Turtle-files containing SHACL-shapes.

![Flagship AP](./visualisation/flagship-ap.png)

An illustration of an exemplary single data point (of all measuring instruments and the machine tool) of a measurement run is given in the [data file](data.ttl) and depicted below.

![Flagship Data](./visualisation/flagship-data.png)

## Remarks

Visualisation of the data graph has been created with [RDF Grapher](https://www.ldf.fi/service/rdf-grapher). The alternative Gaphviz representation (the *.dot*-file) has been generated with [EASY RDF](https://www.easyrdf.org/converter). Visualisation of the AP has been created manualy.

The current version does not contain the heating pads. This part of the AP will be added soon.
