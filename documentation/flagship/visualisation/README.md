# Visualisation

Visualisation of the data graph has been created with [RDF Grapher](https://www.ldf.fi/service/rdf-grapher). The alternative Gaphviz representation (the *.dot*-file) has been generated with [EASY RDF](https://www.easyrdf.org/converter).

Visualisation of the AP has been created manualy.
