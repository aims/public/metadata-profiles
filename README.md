# disclaimer

some of the readme contents may be moved to wiki articles or draft / concept documents in https://git.rwth-aachen.de/aims/wp4/modelling_entities

# file and graph modularization

as suggested in https://spinrdf.org/shacl-and-owl.html, the Application Profile data is split up in a Base Vocabulary *.rdfs.ttl (only classes and properties with labels and sub-class-of relationships), as well as the Shapes *.shacl.ttl in separate files.

> Storing SHACL constraints in the same file as the base vocabulary may make it harder for people to reuse the vocabulary if they disagree with the specific constraints. (property shapes should have URIs so that people can mark them as sh:deactivated true yet it is often more courteous to keep concerns separated).

> With this, we recommend the use of individual graphs when RDF/OWL/SHACL files are published. Note that SHACL includes two properties sh:shapesGraph and sh:suggestedShapesGraph that can be used to link from an RDF Schema or instances file to shapes graphs.

maybe adopt modularization techniques similar to semantic sensor network ontology https://www.w3.org/TR/vocab-ssn/#Modularization

# vocabulary development

Generally, we use [QUDT](https://github.com/qudt/qudt-public-repo/wiki/User-Guide-for-QUDT#1-simple-use-case) to represent units and [DCMI](https://dublincore.org/specifications/dublin-core/dcmi-terms/) for generic terms. SKOS and BFO are used for compatibility with ontology engineering. What about DCAT? What about Metadata4Ing?

the vocabulary graph should contain extra human readable information, SHACL provides properties that dont interfere with validation for this purpose: `sh:name` and `sh:description` (c.f. https://www.w3.org/TR/shacl/#nonValidation)  
http://eis-bonn.github.io/vdbc/ recommends the use of `rdfs:label` and `rdfs:comment` or `skos:prefLabel`, `skos:altLabel` and `skos:definition`  
Unclear which ones should take priority or are redundant. what about `rdfs:domain`, `rdfs:range` or `dcterms:description`, `schema:name` and redundancies with shape constraints?  
Instead of the strong semantic commitment of reusing identifiers from non-authoritative vocabularies, alignments using `owl:sameAs`, `owl:equivalentClass`, `owl:equivalentProperty`, `rdfs:subClassOf`, `rdfs:subPropertyOf` can be established (c.f. http://eis-bonn.github.io/vdbc/).  

There is probably a large value provided to the end user by compiling a rdf-vocabulary from terms of defined in non-machine-readably literature (e.g. metrology handbook)?

Namespaces can be looked up using http://prefix.cc/

Some recommendations regarding ontology development may apply to general vocabulary development https://protege.stanford.edu/publications/ontology_development/ontology101.pdf  
Application profiles and Considerations regarding versioning provided by the Dataset Exchange Working Group (DXWG) can be found at https://www.w3.org/2017/dxwg/wiki/Main_Page  
Some general pointers on how to do open source projects can be found at https://github.com/cornelius/awesome-open-source#development-best-practices

# constraints

we opt to set type constraints via `sh:node foo:BarShape`, rather than `sh:class foo:Bar` because we do not want to rely on the existence of class hierarchies in re-used vocabularies. Sadly this means validations of recursion (therefore also cyclic constraints) cant be done in shacl currently (c.f. https://www.slideshare.net/jelabra/shacl-by-example Slides 19-21).  

# re-use mechanisms

Assuming the above design decisions, end users are provided with the following re-use mechanisms or scenarios:
* direct re-use of terms via IRI 
* indirect re-use of terms via alignment properties like `owl:sameAs`, `rdfs:subClassOf`, etc. (c.f. above)
* direct re-use of shapes and their constraints (non-abstract "Leaf" classes)
* indirect re-use of shapes via new shape definitions that inherit constraints from existing ones
* adding custom properties to existing shapes that are not `sh:closed`

# validation

Validation can be performed and inspected using https://shacl-playground.zazuko.com/ or https://www.ida.liu.se/~robke04/SHACLTutorial/  
Note that the Data Graph also needs to include the Base Vocabulary *.rdfs.ttl data for this to work, c.f. https://github.com/TopQuadrant/shacl-js/issues/22  
But apparently the Shapes Graph does NOT need to include the Base Vocabulary?  
Also implicit target class declaration does seem to be unreliable on the above SHACL Online Editor! Does not detect violations if Base Vocabulary is not included in the data graph? It seems to work reliably on https://shacl.org/playground/  

In research workflows (c.f. published code) we use https://rdflib.readthedocs.io/en/stable/ and https://github.com/RDFLib/pySHACL  

# open questions

* Can we define and resolve dependencies for prefixes / namespaces? e.g. using `@prefix sh: <http://www.w3.org/ns/shacl#> .` implies using `@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .` c.f. http://www.qudt.org/pages/QUDToverviewPage.html "The core schema of QUDT imports the VAEM, DTYPE and SKOS ontologies." 
* Can we alias namespaces? e.g. allow that `<http://www.example.org/aims#some_term>` can be used instead of `<http://www.example.org/some_namespace#some_term>`? i guess we would need to redefine the term in our namespace, then use `owl:sameAs` to point to the original namespace?
* Can IRIs be validated against the declared namespace? e.g. where is it confirmed, that `<http://www.w3.org/2000/01/rdf-schema#subClassOf>` actually exists and is valid?
* Is there a fixed correlation required for the IRIs of Namespace and a serialization? e.g. SHACL namespace is `http://www.w3.org/ns/shacl#` while SHACL serialization can be obtained at `http://www.w3.org/ns/shacl.ttl`. What about `http://qudt.org/schema/qudt/` VS `http://qudt.org/2.1/schema/qudt`?
* Which Graph needs to be merged / included / imported in other Graphs for what reason? c.f. "validation" above

# used resources

A curated list of various semantic web and linked data resources can be found at https://awesomeopensource.com/project/semantalytics/awesome-semantic-web  
Introduction to RDF basics: https://www.w3.org/2007/02/turtle/primer/  
Recommendations for the use of SHACL: https://www.w3.org/TR/shacl/  
Resource used for Assumptions on the Structure of Ontologies and Namespaces: https://www.w3.org/TR/owl-guide/  
Overview of Semantic Web and Linked Data Best Practices, Standards and Metadata Application Profiles: https://guides.library.ucla.edu/semantic-web/bestpractices
